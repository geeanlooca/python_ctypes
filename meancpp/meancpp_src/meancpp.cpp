#include <cblas.h>
#ifdef __linux__
#define DLLEXPORT extern "C"
#include <cstdlib>
#include <cstdio>
#elif _WIN32
#include "pch.h"
#define DLLEXPORT extern "C" __declspec(dllexport)
#endif

extern "C"
{
#include <lapacke.h>
    // #include <lapack.h>
}

DLLEXPORT float mean(const float *data, size_t size)
{
    size_t i;
    float acc = 0;
    for (i = 0; i < size; ++i)
    {
        acc += data[i];
    }

    return acc / size;
}

DLLEXPORT void eigenvalues(float *a, const size_t rows)
{
    float *wr = (float *)malloc(rows * sizeof(float));
    float *wi = (float *)malloc(rows * sizeof(float));
    float *rev = (float *)malloc(rows * sizeof(float));
    float wkopt;
    float *work;
    int lwork = 1;
    int info;
    char lv = 'V';
    char rv = 'N';

    int n = ((int)rows);
    LAPACKE_sgeev(LAPACK_ROW_MAJOR, lv, rv, rows, a, rows, wr, wi, rev, rows, NULL, info);
}

DLLEXPORT float *matrix_multiplication(const float *a, const float *b, size_t rows, size_t cols)
{
    float *c = (float *)malloc(rows * cols * sizeof(float));
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, rows, rows, rows, 1, a, rows, b, rows, 0, c, rows);
    return c;
}