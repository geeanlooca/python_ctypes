import ctypes
import os
import numpy as np
from numpy.ctypeslib import ndpointer

library_ext = ".dll" if os.name == "nt" else ".so"
current_path = os.path.dirname(os.path.abspath(__file__))
library_path = os.path.join(current_path, "build")
_lib = ctypes.cdll.LoadLibrary(os.path.join(library_path, "meancpp" + library_ext))

_mean = _lib.mean
_mean.restype = ctypes.c_float
_mean.argtypes = [ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), ctypes.c_size_t]

_mulmat = _lib.matrix_multiplication
# _mulmat.restype = ndpointer(
# ctypes.c_float,
# )
_mulmat.restype = ctypes.c_void_p


_mulmat.argtypes = [
    ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"),
    ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"),
    ctypes.c_size_t,
    ctypes.c_size_t,
]


def mean(indata):
    return _mean(indata, indata.size)


def matmul(a, b):
    assert a.shape == b.shape
    result = ctypes.cast(
        _mulmat(a, b, a.shape[0], a.shape[1]), ctypes.POINTER(ctypes.c_float)
    )
    return np.ctypeslib.as_array(result, shape=a.shape)
