import numpy as np
import meancpp.meanccp as meanccp
import time
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use("pdf")

A = np.random.randn(100).astype("float32") + 2
print(meanccp.mean(A))


nsize = 4
B = np.random.randn(nsize, nsize).astype("float32") + 2
C = np.random.randn(nsize, nsize).astype("float32") + 2

D_py = np.matmul(B, C)
D_cpp = meanccp.matmul(B, C)

print(D_py)
print(D_cpp)

assert np.allclose(D_py, D_cpp)